import { Task } from "../types/models";
import { mockedTasks } from "../resources/mocked-tasks";
import { ADD_TASK, DELETE_TASK, LOAD_TASKS, TaskAction } from "../actions/tasks.actions";
import * as _ from "lodash";

export const tasksReducer = (state: Task[] = mockedTasks, action: TaskAction) => {
  switch (action.type) {
    case LOAD_TASKS: return action.payload;
    case ADD_TASK: return [...state, action.payload];
    case DELETE_TASK: return _.reject(state, ["id", action.payload]);
    default: return state;
  }
};
