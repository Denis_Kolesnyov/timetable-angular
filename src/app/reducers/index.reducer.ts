import { Task } from "../types/models";
import { dateReducer } from "./date.reducer";
import {tasksReducer} from "./tasks.reducer";

export interface IStoreState {
  selectedDate: string;
  tasks: Task[];
}

export const reducers = {
  selectedDate: dateReducer,
  tasks: tasksReducer
};
