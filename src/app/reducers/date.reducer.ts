import { DateAction, NEXT_DATE, PREV_DATE, RESET_DATE } from "../actions/date.actions";
import * as moment from "moment";
import { getFormattedDate } from "../utils/formatters";

const initialState: string = getFormattedDate();

export const dateReducer = (state: string = initialState, action: DateAction) => {
  switch (action.type) {
    case NEXT_DATE: return getFormattedDate(moment(state).add(1, "d"));
    case PREV_DATE: return getFormattedDate(moment(state).subtract(1, "d"));
    case RESET_DATE: return initialState;
    default: return state;
  }
};
