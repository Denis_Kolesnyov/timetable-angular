import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-weekly-board',
  templateUrl: './weekly-board.component.html',
  styleUrls: ['./weekly-board.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeeklyBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
