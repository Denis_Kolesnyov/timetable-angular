import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyBoardComponent } from './monthly-board.component';

describe('MonthlyBoardComponent', () => {
  let component: MonthlyBoardComponent;
  let fixture: ComponentFixture<MonthlyBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
