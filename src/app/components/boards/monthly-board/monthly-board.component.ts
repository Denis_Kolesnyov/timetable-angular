import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-monthly-board',
  templateUrl: './monthly-board.component.html',
  styleUrls: ['./monthly-board.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonthlyBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
