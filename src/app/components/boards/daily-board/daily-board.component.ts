import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tasks-daily-board',
  templateUrl: './daily-board.component.html',
  styleUrls: ['./daily-board.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DailyBoardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
