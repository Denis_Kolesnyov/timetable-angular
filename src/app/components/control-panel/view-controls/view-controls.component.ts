import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-view-controls',
  templateUrl: './view-controls.component.html',
  styleUrls: ['./view-controls.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewControlsComponent implements OnInit {
  constructor() {  }

  ngOnInit() {  }
}
