import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { IStoreState } from '../../reducers/index.reducer';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/index';
import { getSelectedDate } from '../../selectors/date.selectors';
import {
  GoToNextDateAction,
  GoToPrevDateAction,
  ResetDateAction
} from '../../actions/date.actions';
import {ViewType} from '../../utils/enums';
import {MatDialog} from '@angular/material';
import {TaskEditorComponent} from '../task-editor/task-editor.component';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent implements OnInit {
  selectedDate$: Observable<string>;

  constructor(public store: Store<IStoreState>, public dialog: MatDialog) {
    this.selectedDate$ = store.select(getSelectedDate);
  }

  ngOnInit() {  }

  goToNextDate(): void {
    this.store.dispatch(new GoToNextDateAction());
  }

  goToPrevDate(): void {
    this.store.dispatch(new GoToPrevDateAction());
  }

  resetDate(): void {
    this.store.dispatch(new ResetDateAction());
  }

  openTaskEditor(): void {
    const dialogRef = this.dialog.open(TaskEditorComponent);
  }

}
