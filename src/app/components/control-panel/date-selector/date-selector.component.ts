import {Component, EventEmitter, OnInit, Input, Output, ChangeDetectionStrategy} from '@angular/core';
import {getFormattedDate} from '../../../utils/formatters';

@Component({
  selector: 'date-selector',
  templateUrl: './date-selector.component.html',
  styleUrls: ['./date-selector.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSelectorComponent implements OnInit {
  @Input() selectedDate;
  @Output() goToNextDate = new EventEmitter();
  @Output() goToPrevDate = new EventEmitter();
  @Output() resetDate = new EventEmitter();

  currentDate = getFormattedDate();

  constructor() {}

  ngOnInit() {}

}
