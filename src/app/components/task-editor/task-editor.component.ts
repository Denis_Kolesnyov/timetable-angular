import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { IStoreState } from '../../reducers/index.reducer';
import { Store } from '@ngrx/store';
import { AddTaskAction } from '../../actions/tasks.actions';
import {getFormattedDate} from '../../utils/formatters';
import { Task } from '../../types/models';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-task-editor',
  templateUrl: './task-editor.component.html',
  styleUrls: ['./task-editor.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskEditorComponent implements OnInit {

  constructor(public store: Store<IStoreState>, public dialogRef: MatDialogRef<TaskEditorComponent>) { }

  ngOnInit() { }

  closeEditor(): void {
    this.dialogRef.close();
  }

  addTask() {
    this.store.dispatch(new AddTaskAction(new Task("ololo", getFormattedDate())));
    this.dialogRef.close();
  }
}
