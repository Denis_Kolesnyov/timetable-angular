import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {IStoreState} from "../../reducers/index.reducer";
import {Store} from "@ngrx/store";
import {Task} from "../../types/models";
import {Observable} from "rxjs/index";
import {getTasksForSelectedDate} from "../../selectors/task.selectors";
import {DeleteTaskAction} from "../../actions/tasks.actions";

@Component({
  selector: 'app-task-container',
  templateUrl: './task-container.component.html',
  styleUrls: ['./task-container.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskContainerComponent implements OnInit {
  tasks$: Observable<Task[]>;

  constructor(public store: Store<IStoreState>) {
    this.tasks$ = store.select(getTasksForSelectedDate);
  }

  ngOnInit() { }

  deleteTask(id: number): void {
    this.store.dispatch(new DeleteTaskAction(id));
  }

}
