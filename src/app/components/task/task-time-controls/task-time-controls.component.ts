import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-task-time-controls',
  templateUrl: './task-time-controls.component.html',
  styleUrls: ['./task-time-controls.component.css']
})
export class TaskTimeControlsComponent implements OnInit {
  @Output() deleteTask = new EventEmitter();

  constructor() { }

  ngOnInit() {  }

}
