import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskTimeControlsComponent } from './task-time-controls.component';

describe('TaskTimeControlsComponent', () => {
  let component: TaskTimeControlsComponent;
  let fixture: ComponentFixture<TaskTimeControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskTimeControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTimeControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
