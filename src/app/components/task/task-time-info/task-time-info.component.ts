import { Component, OnInit, Input } from '@angular/core';
import { Task } from "../../../types/models";

@Component({
  selector: 'app-task-time-info',
  templateUrl: './task-time-info.component.html',
  styleUrls: ['./task-time-info.component.css']
})
export class TaskTimeInfoComponent implements OnInit {
  @Input() task: Task;

  constructor() { }

  ngOnInit() {
  }

}
