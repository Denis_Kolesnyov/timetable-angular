import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskTimeInfoComponent } from './task-time-info.component';

describe('TaskTimeInfoComponent', () => {
  let component: TaskTimeInfoComponent;
  let fixture: ComponentFixture<TaskTimeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskTimeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTimeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
