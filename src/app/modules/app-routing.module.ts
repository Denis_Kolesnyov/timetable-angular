import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyBoardComponent } from '../components/boards/daily-board/daily-board.component';
import { WeeklyBoardComponent } from '../components/boards/weekly-board/weekly-board.component';
import { MonthlyBoardComponent } from '../components/boards/monthly-board/monthly-board.component';

export const routes: Routes = [
  { path: '', redirectTo: '/day', pathMatch: 'full' },
  { path: 'day', component: DailyBoardComponent },
  { path: 'week', component: WeeklyBoardComponent },
  { path: 'month', component: MonthlyBoardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
