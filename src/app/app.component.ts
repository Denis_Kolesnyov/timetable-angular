import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/index';
import { IStoreState } from './reducers/index.reducer';
import { getSelectedDate } from './selectors/date.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public selectedDate$: Observable<string>;

  constructor(public store: Store<IStoreState>) {
    this.selectedDate$ = store.select(getSelectedDate);
  }

  ngOnInit() {}
}
