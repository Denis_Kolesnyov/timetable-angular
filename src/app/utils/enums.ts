export enum ViewType {
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH'
}
