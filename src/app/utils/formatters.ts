import * as moment from "moment";

const DEFAULT_DATE_FORMAT = "MM.DD.YYYY";

export function getFormattedDate(date?: string | moment.Moment, format: string = DEFAULT_DATE_FORMAT): string {
  return date
    ? moment(date).format(format)
    : moment().format(format);
}
