import { IStoreState } from "../reducers/index.reducer";
import { Task } from "../types/models";
import { getSelectedDate } from "./date.selectors";

export function getTasks(state: IStoreState): Task[] {
  return state.tasks;
}

export function getTasksForSelectedDate(state: IStoreState): Task[] {
  const tasks = getTasks(state);
  const currentDate = getSelectedDate(state);

  return tasks.filter(({ date }: Task) => date === currentDate);
}

