import { IStoreState } from "../reducers/index.reducer";
import { getFormattedDate } from "../utils/formatters";

export function getSelectedDate(state: IStoreState): string {
  return getFormattedDate(state.selectedDate);
}
