import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers/index.reducer';
import { DateSelectorComponent } from './components/control-panel/date-selector/date-selector.component';
import { ControlPanelComponent } from './components/control-panel/control-panel.component';
import { TaskContainerComponent } from './components/task-container/task-container.component';
import { TaskComponent } from './components/task/task.component';
import { TaskTimeInfoComponent } from './components/task/task-time-info/task-time-info.component';
import { TaskTimeControlsComponent } from './components/task/task-time-controls/task-time-controls.component';
import { AppRoutingModule } from './modules/app-routing.module';
import { DailyBoardComponent } from './components/boards/daily-board/daily-board.component';
import { TaskEditorComponent } from './components/task-editor/task-editor.component';
import { ViewControlsComponent } from './components/control-panel/view-controls/view-controls.component';
import { MaterialModule } from './modules/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WeeklyBoardComponent } from './components/boards/weekly-board/weekly-board.component';
import { MonthlyBoardComponent } from './components/boards/monthly-board/monthly-board.component';

@NgModule({
  declarations: [
    AppComponent,
    DateSelectorComponent,
    ControlPanelComponent,
    TaskContainerComponent,
    TaskComponent,
    TaskTimeInfoComponent,
    TaskTimeControlsComponent,
    DailyBoardComponent,
    TaskEditorComponent,
    ViewControlsComponent,
    WeeklyBoardComponent,
    MonthlyBoardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
