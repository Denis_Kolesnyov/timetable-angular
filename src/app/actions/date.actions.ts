import { Action } from '@ngrx/store';

export const NEXT_DATE = '[Date] Next Date';
export const PREV_DATE = '[Date] Previous Date';
export const RESET_DATE = '[Date] Reset Date';

export class GoToNextDateAction implements Action {
 type = NEXT_DATE;
}

export class GoToPrevDateAction implements Action {
 type = PREV_DATE;
}

export class ResetDateAction implements Action {
 type = RESET_DATE;
}

export type DateAction = GoToNextDateAction | GoToPrevDateAction | ResetDateAction;

