import { Action } from '@ngrx/store';
import { Task } from '../types/models';

export const LOAD_TASKS = '[Tasks] Load Tasks';
export const ADD_TASK = '[Tasks] Add Task';
export const UPDATE_TASK = '[Tasks] Update Task';
export const DELETE_TASK = '[Tasks] Delete Task';

export class LoadTasksAction implements Action {
  type = LOAD_TASKS;

  constructor(public payload: Task[]) {}
}

export class AddTaskAction implements Action {
  type = ADD_TASK;

  constructor(public payload: Task) {}
}

export class UpdateTaskAction implements Action {
  type = UPDATE_TASK;

  constructor(public payload: Task) {}
}

export class DeleteTaskAction implements Action {
  type = DELETE_TASK;

  constructor(public payload: number) {}
}

export type TaskAction = LoadTasksAction | AddTaskAction | UpdateTaskAction | DeleteTaskAction;
