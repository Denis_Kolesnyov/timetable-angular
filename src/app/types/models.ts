import { getFormattedDate } from "../utils/formatters";

export class Task {
  date: string;
  description: string;
  id: number = null;
  creationDate: string = getFormattedDate();
  timeEstimated: string = null;
  timeSpent: string = null;
  startTime: string = null;
  endTime: string = null;

 constructor(description: string, date: string) {
   this.description = description;
   this.date = date;
 }
}
